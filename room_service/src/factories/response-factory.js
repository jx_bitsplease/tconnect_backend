module.exports = {
    generateOkResponse: (data) => {
        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({
                data
            })
        }
    },

    generateRoomOkResponse: (data) => {
        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({
                room: data
            })
        }
    },

    generateRoomsOkResponse: (data) => {
        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({
                rooms: data
            })
        }
    },

    generateErrorResponse: (statusCode, message) => {
        return {
            statusCode,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            body: {
                message
            }
        }
    }
};
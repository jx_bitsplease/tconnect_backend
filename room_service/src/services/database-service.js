const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();
module.exports = {
    queryRooms: async () => {
        const params = {
            ProjectionExpression: 'roomId, roomName',
            TableName: process.env.DYNAMO_TABLE
        };

        const result = await documentClient.scan(params).promise();
        return result.Items;
    },

    queryRoom: async (roomId) => {
        const params = {
            ExpressionAttributeValues: {
                ":id": roomId
            },
            KeyConditionExpression: "roomId = :id",
            TableName: process.env.DYNAMO_TABLE
        };

        const result = await documentClient.query(params).promise();
        return result.Items[0];
    }
};
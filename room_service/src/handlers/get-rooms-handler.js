module.exports.handler = (databaseService, responseFactory) => {
    return async (event) => {
        try {
            const result = await databaseService.queryRooms();
            return responseFactory.generateRoomsOkResponse(result);
        } catch (err) {
            return responseFactory.generateErrorResponse(err.statusCode, err.message);
        }
    }
};
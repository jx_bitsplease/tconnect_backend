module.exports.handler = (databaseService, responseFactory) => {
    return async (event) => {
        const capacity  = event.queryStringParameters.capacity;
        const from = event.queryStringParameters.from;
        const to = event.queryStringParameters.to;



        try {
            const result = await databaseService.queryRoom(roomId);
            return responseFactory.generateRoomOkResponse(result);
        } catch (err) {
            return responseFactory.generateErrorResponse(err.statusCode, err.message);
        }
    }
};
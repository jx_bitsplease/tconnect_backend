module.exports.handler = (databaseService, responseFactory) => {
    return async (event) => {
        const roomId = event.pathParameters.roomId;

        if (!roomId) {
            return responseFactory.generateErrorResponse(400, 'RoomId is required!');
        }

        

        try {
            const result = await databaseService.queryRoom(roomId);
            return responseFactory.generateRoomOkResponse(result);
        } catch (err) {
            return responseFactory.generateErrorResponse(err.statusCode, err.message);
        }
    }
};
const databaseService = require('./src/services/database-service');
const responseFactory = require('./src/factories/response-factory');
const getRooms = require('./src/handlers/get-rooms-handler').handler(databaseService, responseFactory);
const getRoomById = require('./src/handlers/get-room-by-id-handler').handler(databaseService, responseFactory);

module.exports = { getRooms, getRoomById };
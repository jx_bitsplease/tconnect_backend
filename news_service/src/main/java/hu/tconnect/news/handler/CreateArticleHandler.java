package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.news.db.ArticleDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.Article;
import hu.tconnect.news.model.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

public class CreateArticleHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        Article article = new Gson().fromJson((String) input.get("body"), Article.class);
        article.setAuthor(userId);
        article = ArticleDAO.getInstance().createArticle(article);
        return new LambdaResponse(
                false,
                201,
                new HashMap <>(),
                article.getId()
        );
    }
}

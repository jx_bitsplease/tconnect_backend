package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.news.db.NewsTabDAO;
import hu.tconnect.news.model.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

public class GetTabHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String tabId = ((Map <String, Object>)(input.get("pathParameters"))).get("tabId").toString();
        return new LambdaResponse(
                false,
                200,
                new HashMap <>(),
                NewsTabDAO.getInstance().getNewsTab(tabId)
        );
    }
}

package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.news.db.NewsTabDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.LambdaResponse;
import hu.tconnect.news.model.NewsTab;

import java.util.HashMap;
import java.util.Map;

public class CreateTabHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        NewsTab tab = new Gson().fromJson((String) input.get("body"), NewsTab.class);
        tab.setOwner(userId);
        tab = NewsTabDAO.getInstance().createNewsTab(tab);
        return new LambdaResponse(
                false,
                201,
                new HashMap <>(),
                tab.getId()
        );
    }
}

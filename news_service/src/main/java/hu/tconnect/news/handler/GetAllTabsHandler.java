package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.news.db.NewsTabDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

public class GetAllTabsHandler implements RequestHandler<Map<String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        return new LambdaResponse(
                false,
                200,
                new HashMap <>(),
                new Gson().toJson(NewsTabDAO.getInstance().getAllNewsTabByOwnerId(userId))
        );
    }
}

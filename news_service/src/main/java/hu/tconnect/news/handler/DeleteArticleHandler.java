package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.news.db.ArticleDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.Article;
import hu.tconnect.news.model.LambdaResponse;

import java.util.HashMap;
import java.util.Map;

public class DeleteArticleHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String articleId = ((Map <String, Object>)(input.get("pathParameters"))).get("articleId").toString();
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        Article article = ArticleDAO.getInstance().getArticle(articleId);
        if (!article.getAuthor().equals(userId)) {
            return new LambdaResponse(
                    false,
                    403,
                    new HashMap <>(),
                    "You can only delete your own articles"
            );
        }
        ArticleDAO.getInstance().deleteArticle(article);

        return new LambdaResponse(
                false,
                204,
                new HashMap <>(),
                ""
        );
    }
}

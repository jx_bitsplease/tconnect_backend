package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.news.db.NewsTabDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.LambdaResponse;
import hu.tconnect.news.model.NewsTab;

import java.util.HashMap;
import java.util.Map;

public class DeleteTabHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String tabId = ((Map <String, Object>)(input.get("pathParameters"))).get("tabId").toString();
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        NewsTab article = NewsTabDAO.getInstance().getNewsTab(tabId);
        if (!article.getOwner().equals(userId)) {
            return new LambdaResponse(
                    false,
                    403,
                    new HashMap <>(),
                    "You can only delete your own news tabs"
            );
        }
        NewsTabDAO.getInstance().deleteNewsTab(article);

        return new LambdaResponse(
                false,
                204,
                new HashMap <>(),
                ""
        );
    }
}

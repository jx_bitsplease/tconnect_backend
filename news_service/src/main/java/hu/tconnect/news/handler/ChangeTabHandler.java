package hu.tconnect.news.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.news.db.NewsTabDAO;
import hu.tconnect.news.functions.AuthAdapter;
import hu.tconnect.news.model.LambdaResponse;
import hu.tconnect.news.model.NewsTab;

import java.util.HashMap;
import java.util.Map;

public class ChangeTabHandler implements RequestHandler<Map<String, Object>, LambdaResponse> {

    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String tabId = ((Map <String, Object>)(input.get("pathParameters"))).get("tabId").toString();
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        NewsTab tab = NewsTabDAO.getInstance().getNewsTab(tabId);
        if (!tab.getOwner().equals(userId)) {
            return new LambdaResponse(
                    false,
                    403,
                    new HashMap <>(),
                    "You can only change yout own tabs!"
            );
        }
        NewsTab changedTab = new Gson().fromJson((String) input.get("body"), NewsTab.class);
        NewsTabDAO.getInstance().updateNewsTab(tabId, changedTab);
        return new LambdaResponse(
                false,
                200,
                new HashMap <>(),
                "News Tab has been successfully updated"
        );
    }
}

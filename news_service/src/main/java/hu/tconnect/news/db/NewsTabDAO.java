package hu.tconnect.news.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import hu.tconnect.news.model.NewsTab;

import java.util.*;

public class NewsTabDAO {
    private static final String TABLE_NAME = "NewsTab";

    private static NewsTabDAO instance = null;

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;

    public static NewsTabDAO getInstance() {
        if (instance == null) {
            instance = new NewsTabDAO();
        }

        return instance;
    }

    private NewsTabDAO() {
        client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        mapper = new DynamoDBMapper(client);
    }

    public NewsTab getNewsTab(String tabId) {
        return mapper.load(NewsTab.class, tabId);
    }

    public NewsTab createNewsTab(NewsTab tab) {
        tab.setId(UUID.randomUUID().toString());
        mapper.save(tab);
        return tab;
    }

    public Collection<NewsTab> getAllNewsTabByOwnerId(String ownerId) {
        HashMap <String, AttributeValue> eav = new HashMap <>();
        eav.put(":ownerId", new AttributeValue().withS(ownerId));

        DynamoDBScanExpression query = new DynamoDBScanExpression()
                .withFilterExpression("OwnerId = :ownerId")
                .withExpressionAttributeValues(eav);

        PaginatedScanList <NewsTab> results = mapper.scan(NewsTab.class, query);

        if (results.isEmpty()) return null;

        List <NewsTab> tabs = new ArrayList <>(results);

        return tabs;
    }

    public void updateNewsTab(String tabId, NewsTab updatedTab) {
        updatedTab.setId(tabId);
        mapper.save(updatedTab);
    }

    public void deleteNewsTab(NewsTab newsTab) {
        mapper.delete(newsTab);
    }
}

package hu.tconnect.news.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import hu.tconnect.news.model.Article;

import java.util.UUID;

public class ArticleDAO {
    private static final String TABLE_NAME = "Articles";

    private static ArticleDAO instance;

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;

    public static ArticleDAO getInstance() {
        if (instance == null) {
            instance = new ArticleDAO();
        }

        return instance;
    }

    private ArticleDAO() {
        client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        mapper = new DynamoDBMapper(client);
    }

    public Article createArticle(Article article) {
        article.setId(UUID.randomUUID().toString());
        mapper.save(article);
        return article;
    }

    public Article getArticle(String articleId) {
        return mapper.load(Article.class, articleId);
    }

    public void deleteArticle(Article article) {
        mapper.delete(article);
    }

    public void updateArticle(String articleId, Article updatedArticle) {
        updatedArticle.setId(articleId);
        mapper.save(updatedArticle);
    }
}

package hu.tconnect.news.model;

public class ArticleTags {
    private boolean important;
    private boolean it;
    private boolean sales;
    private boolean management;
    private boolean hr;
    private boolean maintenance;
    private boolean cleaning;
    private boolean network;
    private boolean qa;
    private boolean marketing;
    private boolean field;
    private boolean other;

    public ArticleTags() {
    }

    public ArticleTags(boolean important, boolean it, boolean sales, boolean management, boolean hr, boolean maintenance, boolean cleaning, boolean network, boolean qa, boolean marketing, boolean field, boolean other) {
        this.important = important;
        this.it = it;
        this.sales = sales;
        this.management = management;
        this.hr = hr;
        this.maintenance = maintenance;
        this.cleaning = cleaning;
        this.network = network;
        this.qa = qa;
        this.marketing = marketing;
        this.field = field;
        this.other = other;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public boolean isIt() {
        return it;
    }

    public void setIt(boolean it) {
        this.it = it;
    }

    public boolean isSales() {
        return sales;
    }

    public void setSales(boolean sales) {
        this.sales = sales;
    }

    public boolean isManagement() {
        return management;
    }

    public void setManagement(boolean management) {
        this.management = management;
    }

    public boolean isHr() {
        return hr;
    }

    public void setHr(boolean hr) {
        this.hr = hr;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }

    public boolean isCleaning() {
        return cleaning;
    }

    public void setCleaning(boolean cleaning) {
        this.cleaning = cleaning;
    }

    public boolean isNetwork() {
        return network;
    }

    public void setNetwork(boolean network) {
        this.network = network;
    }

    public boolean isQa() {
        return qa;
    }

    public void setQa(boolean qa) {
        this.qa = qa;
    }

    public boolean isMarketing() {
        return marketing;
    }

    public void setMarketing(boolean marketing) {
        this.marketing = marketing;
    }

    public boolean isField() {
        return field;
    }

    public void setField(boolean field) {
        this.field = field;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }
}

package hu.tconnect.news.functions;

import com.google.gson.Gson;
import hu.tconnect.news.model.TokenDTO;

import java.util.Base64;
import java.util.Map;

public class AuthAdapter {
    public static String getAccountIdFromEvent(Map<String, Object> event) {
        TokenDTO token = new Gson().fromJson(
                new String(
                        Base64.getDecoder().decode(
                                (((Map <String, Object>) event.get("headers"))
                                        .get("Authorization")
                                        .toString()
                                ).split("\\.")[1])
                ), TokenDTO.class);
        return token.getSub();
    }
}


package hu.tconnect.skill.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import hu.tconnect.skill.models.Skill;

import java.util.Collection;
import java.util.UUID;

public class SkillDAO {
    private static final String TABLE_NAME = "Skill";

    private static SkillDAO instance = null;

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;

    public static SkillDAO getInstance() {
        if (instance == null) {
            instance = new SkillDAO();
        }
        return instance;
    }

    private SkillDAO() {
        client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        mapper = new DynamoDBMapper(client);
    }

    public Collection<Skill> getAllSkills() {
        return mapper.scan(Skill.class, new DynamoDBScanExpression());
    }

    public Skill createSkill(Skill skill) {
        skill.setId(UUID.randomUUID().toString());
        mapper.save(skill);
        return skill;
    }

    public Skill getSkill(Skill skill) {
        return mapper.load(Skill.class, skill.getId());
    }

}

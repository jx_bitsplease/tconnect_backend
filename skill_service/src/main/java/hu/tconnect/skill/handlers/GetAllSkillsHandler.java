package hu.tconnect.skill.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.skill.db.SkillDAO;
import hu.tconnect.skill.models.LambdaResponse;

import java.util.HashMap;

public class GetAllSkillsHandler implements RequestHandler<Object, LambdaResponse> {
    public LambdaResponse handleRequest(Object input, Context context) {
        return new LambdaResponse(
                false,
                200,
                new HashMap <String, String>(),
                SkillDAO.getInstance().getAllSkills()
        );
    }
}

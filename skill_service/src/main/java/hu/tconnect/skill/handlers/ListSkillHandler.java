package hu.tconnect.skill.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.skill.db.SkillDAO;
import hu.tconnect.skill.models.LambdaResponse;
import hu.tconnect.skill.models.Skill;

import java.util.*;

public class ListSkillHandler implements RequestHandler<Collection<Skill>, LambdaResponse> {
    public LambdaResponse handleRequest(Collection <Skill> input, Context context) {
        List<Skill> skills = new ArrayList <Skill>();
        for (Skill skill: input) {
            skills.add(SkillDAO.getInstance().getSkill(skill));
        }
        return new LambdaResponse(
                false,
                200,
                new HashMap <String, String>(),
                skills
        );
    }
}

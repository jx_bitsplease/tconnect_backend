package hu.tconnect.skill.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.skill.db.SkillDAO;
import hu.tconnect.skill.models.LambdaResponse;
import hu.tconnect.skill.models.Skill;

import java.util.HashMap;

public class CreateSkillHandler implements RequestHandler<Skill, LambdaResponse> {
    public LambdaResponse handleRequest(Skill input, Context context) {
        return new LambdaResponse(
                false,
                201,
                new HashMap <String, String>(),
                SkillDAO.getInstance().createSkill(input).getId()
        );
    }
}

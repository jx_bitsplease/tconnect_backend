package hu.tconnect.skill.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import hu.tconnect.skill.db.SkillDAO;
import hu.tconnect.skill.models.LambdaResponse;
import hu.tconnect.skill.models.Skill;

import java.util.HashMap;

public class GetSkillHandler implements RequestHandler<String, LambdaResponse> {

    @Override
    public LambdaResponse handleRequest(String input, Context context) {
        return new LambdaResponse(
                false,
                200,
                new HashMap <String, String>(),
                SkillDAO.getInstance().getSkill(new Skill(input, null, null))
        );
    }
}

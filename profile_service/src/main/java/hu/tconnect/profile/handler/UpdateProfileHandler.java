package hu.tconnect.profile.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.profile.db.ProfileDAO;
import hu.tconnect.profile.function.AuthAdapter;
import hu.tconnect.profile.model.LambdaResponse;
import hu.tconnect.profile.model.Profile;

import java.util.HashMap;
import java.util.Map;

public class UpdateProfileHandler implements RequestHandler<Map<String, Object>, LambdaResponse> {
    public LambdaResponse handleRequest(Map<String, Object> input, Context context) {
        String profileId = ((Map <String, Object>)(input.get("pathParameters"))).get("profileId").toString();
        String userId = AuthAdapter.getAccountIdFromEvent(input);
        Profile profile = ProfileDAO.getInstance().getProfile(profileId);
        if (!profile.getAccountId().equals(userId)) {
            return new LambdaResponse(
                    false,
                    403,
                    new HashMap <>(),
                    new Gson().toJson("You can only change your own profile!")
            );
        }
        Profile changedProfile = new Gson().fromJson((String) input.get("body"), Profile.class);
        ProfileDAO.getInstance().updateProfile(profileId, changedProfile);
        return new LambdaResponse(
                false,
                200,
                new HashMap <>(),
                new Gson().toJson("Profile has been successfully updated")
        );

    }
}
package hu.tconnect.profile.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.profile.db.ProfileDAO;
import hu.tconnect.profile.model.LambdaResponse;
import hu.tconnect.profile.model.Profile;

import java.util.HashMap;
import java.util.Map;

public class GetProfileHandler implements RequestHandler<Map <String, Object>, LambdaResponse> {
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        Profile profile = ProfileDAO.getInstance().getProfile(((Map <String, Object>)(input.get("pathParameters"))).get("profileId").toString());
        if (profile != null) {
            return new LambdaResponse(
                    false,
                    200,
                    new HashMap <String, String>(),
                    new Gson().toJson(profile)
            );
        } else {
            return new LambdaResponse(
                    false,
                    404,
                    new HashMap <String, String>(),
                    new Gson().toJson("Profile not found")
            );
        }
    }
}
package hu.tconnect.profile.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.profile.db.ProfileDAO;
import hu.tconnect.profile.function.AuthAdapter;
import hu.tconnect.profile.model.LambdaResponse;
import hu.tconnect.profile.model.Profile;

import java.util.HashMap;
import java.util.Map;

public class CreateProfileHandler implements RequestHandler<Map<String, Object>, LambdaResponse> {

    public LambdaResponse handleRequest(Map<String, Object> input, Context context) {
        Profile profile = new Gson().fromJson((String) input.get("body"), Profile.class);
        profile.setAccountId(AuthAdapter.getAccountIdFromEvent(input));
        profile = ProfileDAO.getInstance().saveProfile(profile);
        return new LambdaResponse(
                false,
                201,
                new HashMap <>(),
                new Gson().toJson(profile.getId())
        );
    }
}

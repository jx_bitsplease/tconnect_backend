package hu.tconnect.profile.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import hu.tconnect.profile.db.ProfileDAO;
import hu.tconnect.profile.function.AuthAdapter;
import hu.tconnect.profile.model.LambdaResponse;
import hu.tconnect.profile.model.Profile;

import java.util.HashMap;
import java.util.Map;

public class GetCurrentProfileHandler implements RequestHandler<Map<String, Object>, LambdaResponse> {

    @Override
    public LambdaResponse handleRequest(Map <String, Object> input, Context context) {
        String accountId = AuthAdapter.getAccountIdFromEvent(input);
        return new LambdaResponse(
                false,
                200,
                new HashMap <>(),
                new Gson().toJson(ProfileDAO.getInstance().getProfileByAccountId(accountId))
        );
    }
}

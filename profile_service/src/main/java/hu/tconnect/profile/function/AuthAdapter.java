package hu.tconnect.profile.function;

import com.google.gson.Gson;
import hu.tconnect.profile.model.TokenDTO;

import java.util.Base64;
import java.util.Map;

public class AuthAdapter {
    public static String getAccountIdFromEvent(Map<String, Object> event) {
        TokenDTO token = new Gson().fromJson(
                new String(
                        Base64.getDecoder().decode(
                                (((Map <String, Object>) event.get("headers"))
                                        .get("Authorization")
                                        .toString()
                                ).split("\\.")[1])
                ), TokenDTO.class);
        return token.getSub();
    }
}

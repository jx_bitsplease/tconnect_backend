package hu.tconnect.profile.model;

import java.util.HashMap;
import java.util.Map;

public class LambdaResponse {
    private boolean isBase64Encoded;
    private int statusCode;
    private Map<String, String> headers;
    private Object body;

    public LambdaResponse(boolean isBase64Encoded, int statusCode, Map <String, String> headers, Object body) {
        Map<String, String> baseHeaders = new HashMap <>();
        baseHeaders.put("Access-Control-Allow-Origin", "*");
        baseHeaders.putAll(headers);
        this.isBase64Encoded = isBase64Encoded;
        this.statusCode = statusCode;
        this.headers = baseHeaders;
        this.body = body;
    }

    public boolean isBase64Encoded() {
        return isBase64Encoded;
    }

    public void setBase64Encoded(boolean base64Encoded) {
        isBase64Encoded = base64Encoded;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Map <String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map <String, String> headers) {
        this.headers = headers;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

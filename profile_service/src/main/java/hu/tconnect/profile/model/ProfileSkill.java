package hu.tconnect.profile.model;

public class ProfileSkill {
    private String skillId;

    private Integer level;

    public ProfileSkill() {
    }

    public ProfileSkill(String skillId, Integer level) {
        this.skillId = skillId;
        this.level = level;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

}

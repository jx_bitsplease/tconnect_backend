package hu.tconnect.profile.model;

public class Socials {
    private String linkedInUrl;

    private String facebookUrl;

    private String twitterUrl;

    private String gitHubUrl;

    Socials() {
    }

    Socials(String linkedInUrl, String facebookUrl, String twitterUrl, String gitHubUrl) {
        this.linkedInUrl = linkedInUrl;
        this.facebookUrl = facebookUrl;
        this.twitterUrl = twitterUrl;
        this.gitHubUrl = gitHubUrl;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public String getGitHubUrl() {
        return gitHubUrl;
    }

    public void setGitHubUrl(String gitHubUrl) {
        this.gitHubUrl = gitHubUrl;
    }

    @Override
    public String toString() {
        return "Socials{" +
                "linkedInUrl='" + linkedInUrl + '\'' +
                ", facebookUrl='" + facebookUrl + '\'' +
                ", twitterUrl='" + twitterUrl + '\'' +
                ", gitHubUrl='" + gitHubUrl + '\'' +
                '}';
    }
}

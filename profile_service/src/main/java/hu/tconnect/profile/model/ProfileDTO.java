package hu.tconnect.profile.model;

import java.util.ArrayList;
import java.util.List;

public class ProfileDTO {
    private String id;

    private String accountId;

    private String name;

    private String avatar;

    private String email;

    private String position;

    private String phone;

    private Socials socials = new Socials();

    private String deskNumber;

    private String roomNumber;

    private List <ProfileSkill> profileSkills = new ArrayList <ProfileSkill>();
}

package hu.tconnect.profile.model;

public class TokenDTO {
    private String sub;

    public TokenDTO(String sub) {
        this.sub = sub;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }
}

package hu.tconnect.profile.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import hu.tconnect.profile.model.Profile;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ProfileDAO {
    private static final String TABLE_NAME = "Profile";

    private static ProfileDAO instance = null;

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;

    public static ProfileDAO getInstance() {
        if (instance == null) {
            instance = new ProfileDAO();
        }
        return instance;
    }

    private ProfileDAO() {
        client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        mapper = new DynamoDBMapper(client);
    }

    public Profile saveProfile(Profile profile) {
        profile.setId(UUID.randomUUID().toString());
        mapper.save(profile);
        return profile;
    }

    public Profile getProfile(String profileId) {
        return mapper.load(Profile.class, profileId);
    }

    public Profile getProfileByAccountId(String accountId) {
        HashMap<String, AttributeValue> eav = new HashMap <>();
        eav.put(":accountId", new AttributeValue().withS(accountId));

        DynamoDBScanExpression query = new DynamoDBScanExpression()
            .withFilterExpression("AccountId = :accountId")
            .withExpressionAttributeValues(eav);

        PaginatedScanList <Profile> profiles = mapper.scan(Profile.class, query);

        if (profiles.isEmpty()) return null;

        return profiles.iterator().next();
    }

    public Profile updateProfile(String profileId, Profile profile) {
        profile.setId(profileId);
        mapper.save(profile);
        return profile;
    }
}
